import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import { ValidationPipes } from './common/pipes/validation.pipe';
import { createDocument } from './config/swagger/swagger';

/**
 * declaring mosule
 */
declare const module: any;

/**
 * starting point of the application
 */
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // app.useGlobalPipes(new ValidationPipes());

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.setGlobalPrefix('api/v1');

  SwaggerModule.setup('api', app, createDocument(app));

  app.use(cookieParser());
  app.enableCors({
    origin: 'http://localhost:8080',
    credentials: true
  });
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  await app.listen(3000);
}
bootstrap();
