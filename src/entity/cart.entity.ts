import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { UserLogin } from './login.entity';

/**
 * cart entity
 */
@Entity()
export class Cart {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  userId: number;

  @ApiProperty()
  @IsString()
  @Column()
  categoryName: string;

  @ApiProperty()
  @IsString()
  @Column()
  itemName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  quantity: number;

  @Column()
  price: number;

  @Column()
  totalPrice: number;

  @OneToOne(() => UserLogin, (login) => login.cart, {
    cascade: true
  })
  @JoinColumn()
  login: UserLogin;
}
