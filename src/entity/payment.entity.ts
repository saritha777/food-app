import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { UserLogin } from './login.entity';

/**
 * payment entity
 */
@Entity()
export class Payment {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  @IsInt()
  userId: number;

  @Column()
  email: string;

  @ApiProperty()
  @IsInt()
  @Column()
  cartId: number;

  @ApiProperty()
  @IsInt()
  @Column()
  cardNumber: number;

  @ApiProperty()
  @IsInt()
  @Column()
  cvv: number;

  @ApiProperty()
  @IsString()
  @Column()
  expiry: string;

  @ApiProperty()
  @IsString()
  @Column()
  cardHolderName: string;

  @OneToOne(() => UserLogin, (login) => login.payment, {
    cascade: true
  })
  @JoinColumn()
  login: UserLogin;
}
