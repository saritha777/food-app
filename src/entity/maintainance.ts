import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsString } from 'class-validator';
import { Column } from 'typeorm';

/**
 * maintainance class
 */
export class Maintainance {
  /**
   * created By property
   */
  @ApiProperty()
  @IsString()
  @Column()
  createdBy: string;

  /**
   * created date
   */
  @ApiProperty()
  @IsString()
  @Column()
  createdDate: string;

  /**
   * updated by property
   */
  @ApiProperty()
  @IsString()
  @Column()
  updatedBy: string;

  /**
   * updated date property
   */
  @ApiProperty()
  @IsString()
  @Column()
  updatedDate: string;
}
