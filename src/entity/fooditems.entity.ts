import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Category } from './category';
import { Maintainance } from './maintainance';

/**
 * food items entity
 */
@Entity()
export class FoodItems extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  categoryName: string;

  @ApiProperty()
  @IsString()
  @Column()
  itemName: string;

  @ApiProperty()
  @IsString()
  @Column()
  description: string;

  @ApiProperty()
  @IsInt()
  @Column()
  price: number;

  @ManyToOne(() => Category, (category) => category.foodItems, {
    cascade: true
  })
  @JoinColumn()
  category: Category;
}
