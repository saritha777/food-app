import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsInt,
  IsString,
  MaxLength,
  MinLength
} from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';
import { Maintainance } from './maintainance';

/**
 * user entity
 */
@Entity()
export class User extends Maintainance {
  /**
   * id as primary generated column
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * name property
   */
  @ApiProperty()
  @IsString()
  @Column()
  firstName: string;

  /**
   * lasr name property
   */
  @ApiProperty()
  @IsString()
  @Column()
  lastName: string;

  /**
   * email property
   */
  @ApiProperty()
  @IsEmail()
  @Column({ unique: true })
  email: string;

  /**
   * password property
   */
  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(10)
  @Column({ unique: true })
  password: string;

  /**
   * phone no property
   */
  @ApiProperty()
  @IsInt()
  @Column()
  phoneNo: number;

  /**
   * address property
   */
  @ApiProperty()
  @IsString()
  @Column()
  address: string;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true
  })
  login: UserLogin;
}
