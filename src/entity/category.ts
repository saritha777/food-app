import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { FoodItems } from './fooditems.entity';

/**
 * category entity
 */
@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  categoryName: string;

  @OneToMany(() => FoodItems, (foodItems) => foodItems.category, {})
  foodItems: FoodItems;
}
