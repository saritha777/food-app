import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Cart } from '../entity/cart.entity';

import { User } from '../entity/user.entity';
import { Payment } from './payment.entity';

/**
 * user login entity
 */
@Entity()
export class UserLogin {
  /**
   * primary generated column for the id
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * email property
   */
  @ApiProperty()
  @IsString()
  @Column()
  email: string;

  /**
   * password property
   */
  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  @OneToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  user: User;

  @OneToOne(() => Cart, (cart) => cart.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  cart: Cart;

  @OneToOne(() => Payment, (payment) => payment.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  payment: Payment;
}
