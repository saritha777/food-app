import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Category } from '../../entity/category';
import { CategoryService } from './category.service';

/**
 * category controller for passing the data
 */
@ApiTags('category')
@Controller('/category')
export class CategoryController {
  /**
   * injecting category service
   * @param categoryService injecting category service for business logics
   */
  constructor(private readonly categoryService: CategoryService) {}

  /**
   * post method for category
   * @param category taking category details
   * @returns saving category details to database
   */
  @Post()
  async addCategory(@Body() category: Category) {
    return await this.categoryService.addCategory(category);
  }
}
