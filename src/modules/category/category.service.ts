import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from '../../entity/category';
import { Repository } from 'typeorm';

/**
 * category service for business logics
 */
@Injectable()
export class CategoryService {
  /**
   * injecting repository
   * @param categoryRepository taking category Repository for storing
   */
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>
  ) {}

  /**
   * add method for category
   * @param data taking category as input
   * @returns category details
   */
  async addCategory(data: Category) {
    return await this.categoryRepository.save(data);
  }
}
