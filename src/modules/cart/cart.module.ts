import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserLogin } from '../../entity/login.entity';
import { Cart } from '../../entity/cart.entity';

import { FoodItems } from '../../entity/fooditems.entity';

import { CartController } from './cart.controller';
import { CartService } from './cart.service';

@Module({
  imports: [TypeOrmModule.forFeature([Cart, UserLogin, FoodItems])],
  controllers: [CartController],
  providers: [CartService]
})
export class CartModule {}
