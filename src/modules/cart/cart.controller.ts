import { Body, Controller, Delete, Param, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Cart } from '../../entity/cart.entity';
import { CartService } from './cart.service';

/**
 * cart controller
 */
@ApiTags('Cart')
@Controller('/Cart')
export class CartController {
  /**
   * injecting cart service
   * @param cartService of CartService is added
   */
  constructor(private readonly cartService: CartService) {}

  /**
   * post method for cart
   * @param id taking id from user
   * @param cart taking cart dat
   * @returns adding items to cart
   */
  @Post('/cart')
  addCart(@Body() cart: Cart) {
    return this.cartService.addItemsIntoCart(cart);
  }

  /**
   * delete method for
   * @param userId
   * @returns
   */
  @Delete('/cart/:userId')
  delete(@Param('userId') userId: number) {
    return this.cartService.delete(userId);
  }
}
