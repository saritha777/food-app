import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from '../../entity/cart.entity';
import { FoodItems } from '../../entity/fooditems.entity';
import { UserLogin } from '../../entity/login.entity';
import { getManager, Repository } from 'typeorm';

/**
 * Having all business logics related to cart
 */
@Injectable()
export class CartService {
  /**
   *injecting repositories
   * @param cartRepository cart repository
   * @param loginRepository login repository
   * @param foodItemsRepository food items repository
   * @returns all cart details
   */
  constructor(
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    @InjectRepository(FoodItems)
    private foodItemsRepository: Repository<FoodItems>
  ) {}

  /**
   *add method for adding cart details
   * @param id taking id from user data
   * @param data taking cart details
   * @returns cart details
   */
  async addItemsIntoCart(data: Cart) {
    const cartData = new Cart();

    const loginData: UserLogin = await this.loginRepository.findOne({
      id: data.userId
    });
    cartData.userId = loginData.id;
    const itemsData = await this.foodItemsRepository.findOne({
      categoryName: data.categoryName
    });

    cartData.categoryName = itemsData.categoryName;
    cartData.itemName = data.itemName = itemsData.itemName;
    cartData.login = loginData;
    cartData.price = itemsData.price;
    cartData.quantity = data.quantity;
    cartData.totalPrice = cartData.price * cartData.quantity;
    await this.cartRepository.save(cartData);
    const allCartItems = await this.cartRepository.find();
    const amount = await getManager().query(
      `SELECT SUM(totalPrice) "price" FROM cart where userId = "${cartData.userId}";`
    );

    const item = amount[0];
    const result = {
      Allitems: allCartItems,
      priceMessage: 'Total price in your cart items',
      priceValue: item.price
    };
    return result;
  }

  /**
   *delete method
   * @param userId based on user Id
   * @returns delted data based on user id
   */
  async delete(userId: number) {
    return await this.cartRepository.delete({ userId: userId });
  }
}
