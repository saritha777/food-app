import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtToken } from '../../common/providers/jwtservice';
import { UserLogin } from '../../entity/login.entity';
import { User } from '../../entity/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Response, Request } from 'express';

/**
 * Providers can be injected into other classes via constructor parameter injection
 */
@Injectable()
export class UserService {
  /**
   *all repositories is injected in constructor
   * @param userRepository user is injected by user repository
   * @param loginRepository Reg is injected by login repository
   * @param jwtToken jwtToken is instance of userservice and returns jwtToken
   */
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    private jwtToken: JwtToken
  ) {}

  //call the generate token function
  //request object. and set cookie value
  /**
   * write adduser() method to add some details
   * @param data assign user to param
   * @returns userdata
   */
  async addUser(data: User): Promise<User> {
    const pass = await bcrypt.hash(data.password, 10);
    const userData = new User();
    const loginData = new UserLogin();
    userData.firstName = data.firstName;
    userData.lastName = data.lastName;
    userData.email = loginData.email = data.email;
    userData.password = loginData.password = pass;
    userData.phoneNo = data.phoneNo;
    userData.address = data.address;
    userData.createdBy = data.createdBy;
    userData.createdDate = data.createdDate;
    userData.updatedBy = data.updatedBy;
    userData.updatedDate = data.updatedDate;
    userData.login = loginData;

    return this.userRepository.save(userData);
  }

  /**
   * user is login
   * @param data pass Reg data
   * @param response getting response
   * @param req giving request
   * @returns name
   */
  async login(data: UserLogin, response: Response, req: Request) {
    const user = await this.loginRepository.findOne({
      email: data.email
    });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('invalid ');
    }
    const jwt = await this.jwtToken.generateToken(user);
    response.cookie('jwt', jwt, { httpOnly: true });
    console.log('printing');
    const cookie = req.cookies['jwt'];
    const a = await this.jwtToken.verifyToken(cookie);
    return a;
  }

  /**
   * write getAll() method for all user details
   * @returns getting all details of user
   */
  async getAll(): Promise<User[]> {
    return this.userRepository.find();
  }
}
