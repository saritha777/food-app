import { Body, Controller, Get, Post, Req, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserLogin } from '../../entity/login.entity';
import { User } from '../../entity/user.entity';
import { UserService } from './user.service';
import { Request, Response } from 'express';

/**
 * controller that can receive inbound requests and produce responses
 */
@ApiTags('user')
@Controller('/user')
export class UserController {
  /**
   *injecting user service
   * @param userService
   */
  constructor(private readonly userService: UserService) {}
  /**
   * write adduser method to get user details
   * @param user user entity taken as parameter
   * @returns added user details
   */
  @Post()
  async addUser(@Body() user: User): Promise<User> {
    return await this.userService.addUser(user);
  }

  /**
   * post method for login
   * @param login taking login data
   * @param response taking response
   * @param request getting request obj
   * @returns string the user is loged in or not
   */
  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response,
    @Req() request: Request
  ) {
    return await this.userService.login(login, response, request);
  }

  /**
   * get method for all users
   * @returns all users
   */
  @Get()
  async getAll() {
    return this.userService.getAll();
  }
}
