import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from '../../entity/category';
import { FoodItems } from '../../entity/fooditems.entity';
import { Repository } from 'typeorm';

/**
 * food items service having all business logics
 */
@Injectable()
export class FoodItemsService {
  /**
   * injecting repositories
   * @param foodItemsRepository injecting food items repository
   * @param categoryRepository injecting category repository
   */
  constructor(
    @InjectRepository(FoodItems)
    private foodItemsRepository: Repository<FoodItems>,
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>
  ) {}

  /**
   * add method for adding food items
   * @param categoryId taking category id for category name
   * @param data passing food item details
   * @returns 1food item details
   */
  async addFoodItems(categoryId: number, data: FoodItems) {
    const foodItemsData = new FoodItems();
    const categoryData = await this.categoryRepository.findOne({
      id: categoryId
    });
    const categoryType = (data.categoryName = categoryData.categoryName);
    foodItemsData.categoryName = categoryType;
    foodItemsData.createdBy = data.createdBy;
    foodItemsData.createdDate = data.createdDate;
    foodItemsData.description = data.description;
    foodItemsData.itemName = data.itemName;
    foodItemsData.price = data.price;
    foodItemsData.updatedBy = data.updatedBy;
    foodItemsData.updatedDate = data.updatedDate;

    return await this.foodItemsRepository.save(foodItemsData);
  }

  /**
   * get method for all food items
   * @returns all food items
   */
  async getAllFoodItems() {
    return await this.foodItemsRepository.find();
  }

  /**
   * get method based on item name
   * @param itemName taking item name for getting item Name
   * @returns item details based on item name
   */
  async getFoodItemsByName(itemName: string) {
    return await this.foodItemsRepository.findOne({ itemName: itemName });
  }

  /**
   * update method for updating food items
   * @param id taking id which we need to update
   * @param categoryId taking category id for category name from that
   * @param data taking food items to update
   * @returns updated result
   */
  async updateFoodItems(id: number, categoryId: number, data: FoodItems) {
    const foodItemsData = new FoodItems();
    const categoryData = await this.categoryRepository.findOne({
      id: categoryId
    });
    const categoryType = (data.categoryName = categoryData.categoryName);
    foodItemsData.categoryName = categoryType;
    foodItemsData.createdBy = data.createdBy;
    foodItemsData.createdDate = data.createdDate;
    foodItemsData.description = data.description;
    foodItemsData.itemName = data.itemName;
    foodItemsData.price = data.price;
    foodItemsData.updatedBy = data.updatedBy;
    foodItemsData.updatedDate = data.updatedDate;

    await this.foodItemsRepository.update({ id: id }, data);
    return 'updated item details successfully';
  }

  /**
   * delete method
   * @param id taking id to delete
   * @returns deleted information
   */
  async deleteFoodItems(id: number) {
    return await this.foodItemsRepository.delete({ id: id });
  }
}
