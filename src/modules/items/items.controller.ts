import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { FoodItems } from '../../entity/fooditems.entity';
import { FoodItemsService } from './items.service';

/**
 * food items controller
 */
@ApiTags('FOODITEMS')
@Controller('/fooditems')
export class FoodItemsController {
  /**
   * injecting food items service for business logic
   * @param foodItemsService injecting food items service
   */
  constructor(private readonly foodItemsService: FoodItemsService) {}

  /**
   * add method for adding food items
   * @param categoryId taking category id
   * @param data taking food items
   * @returns food details
   */
  @Post('/:id/:categoryId')
  async addFoodItems(
    @Param('categoryId') categoryId: number,
    @Body()
    data: FoodItems
  ) {
    return await this.foodItemsService.addFoodItems(categoryId, data);
  }

  /**
   * get all food items
   * @returns all food items
   */
  @Get()
  async getAllFoodItems(): Promise<FoodItems[]> {
    return await this.foodItemsService.getAllFoodItems();
  }

  /**
   * update method for food items update
   * @param id taking id for update
   * @param categoryId taking category id
   * @param data taking food items for update
   * @returns updated information
   */
  @Put('/:id/:categoryId')
  async updateFoodItems(
    @Param('id') id: number,
    @Param('categoryId') categoryId: number,
    @Body()
    data: FoodItems
  ) {
    return await this.foodItemsService.updateFoodItems(id, categoryId, data);
  }

  /**
   * delete method to delete food items
   * @param id taking id to delete
   * @returns deleted information
   */
  @Delete('/:id')
  async deleteFoodItems(@Param('id') id: number) {
    return this.foodItemsService.deleteFoodItems(id);
  }
}
