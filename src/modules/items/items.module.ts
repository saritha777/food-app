import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from '../../entity/category';
import { FoodItems } from '../../entity/fooditems.entity';
import { FoodItemsController } from './items.controller';
import { FoodItemsService } from './items.service';

@Module({
  imports: [TypeOrmModule.forFeature([FoodItems, Category])],
  controllers: [FoodItemsController],
  providers: [FoodItemsService]
})
export class FoodItemsModule {}
