import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from '../../entity/cart.entity';
import { UserLogin } from '../../entity/login.entity';
import { Payment } from '../../entity/payment.entity';
import { Repository } from 'typeorm';

/**
 * payment service
 */
@Injectable()
export class PaymentService {
  /**
   * all repositories are injected in constructor
   * @param paymentRepository passing payment as param
   * @param userRepository passing user as param
   * @param cartRepository passing cart as param
   */
  constructor(
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>
  ) {}
  /**
   * we add all the user details by passing user
   * @param payment passing payment as param
   * @returns adding payment Details
   */
  async addPayment(payment: Payment) {
    const paymentData = new Payment();
    const loginData: UserLogin = await this.loginRepository.findOne({
      id: payment.userId
    });
    const cartResult: Cart = await this.cartRepository.findOne({
      id: payment.cartId
    });
    paymentData.login = loginData;
    paymentData.userId = loginData.id;
    paymentData.email = loginData.email;
    paymentData.cardNumber = payment.cardNumber;
    paymentData.cvv = payment.cvv;
    paymentData.cartId = cartResult.id;
    paymentData.expiry = payment.expiry;
    paymentData.cardHolderName = payment.cardHolderName;
    return await this.paymentRepository.save(paymentData);
  }
}
