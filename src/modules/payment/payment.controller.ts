import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Payment } from '../../entity/payment.entity';
import { PaymentService } from './payment.service';

/**
 * having all business logics related to payment
 */
@ApiTags('Payment')
@Controller('Payment')
export class PaymentController {
  /**
   * injecting payment repository
   * @param paymentService injecting payment service
   */
  constructor(private readonly paymentService: PaymentService) {}
  /**
   * we declare add method for adding below details by passing payment as param
   * @param payment passing payment as param
   * @returns adding payment Details
   */
  @Post('/payment')
  addPayment(@Body() payment: Payment): Promise<Payment> {
    return this.paymentService.addPayment(payment);
  }
}
