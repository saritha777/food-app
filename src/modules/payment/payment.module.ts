import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cart } from '../../entity/cart.entity';
import { UserLogin } from '../../entity/login.entity';
import { Payment } from '../../entity/payment.entity';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';

@Module({
  imports: [TypeOrmModule.forFeature([Payment, UserLogin, Cart])],
  controllers: [PaymentController],
  providers: [PaymentService]
})
export class PaymentModule {}
