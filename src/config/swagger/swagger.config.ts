import { SwaggerConfig } from './swagger.interface';

/**
 * swagger config object
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  /**
   * title property
   */
  title: 'FOOD - APPLICATION',
  /**
   * description property
   */
  description: 'food application',
  /**
   * version property
   */
  version: '1.0',
  /**
   * tags property
   */
  tags: ['templete'],
};
