import { Injectable } from '@nestjs/common';

/**
 * app service
 */
@Injectable()
export class AppService {
  /**
   * get method for hello world
   * @returns hello world as output
   */
  getHello(): string {
    return 'Hello World!';
  }
}
