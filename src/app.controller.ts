import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

/**
 * app controller
 */
@Controller()
export class AppController {
  /**
   * app service having all business logics
   * @param appService injecting app service
   */
  constructor(private readonly appService: AppService) {}

  /**
   * get method for hello
   * @returns ouput from app service
   */
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
