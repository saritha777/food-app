import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

/**
 * logger middele ware method
 */
@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  /**
   * creating logger object
   */
  private logger = new Logger('food-app');

  /**
   * use methd
   * @param request taking request
   * @param response taking response
   * @param next for next execution
   */
  use(request: Request, response: Response, next: NextFunction): void {
    const { ip, method, originalUrl } = request;
    /**
     * user agent onject
     */
    const userAgent = request.get('user-agent') || '';

    response.on('finish', () => {
      const { statusCode } = response;
      const contentLength = response.get('content-length');

      this.logger.log(
        `${method} ${originalUrl} ${statusCode} ${contentLength}-${userAgent}${ip}`,
      );
    });

    next();
  }
}
