'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">food-app documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' : 'data-target="#xs-controllers-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' :
                                            'id="xs-controllers-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' : 'data-target="#xs-injectables-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' :
                                        'id="xs-injectables-links-module-AppModule-71068fd1946bbf4b1bf95bb644575a1e36333b72e8e1edf424cb19f04c7db6970fe40f582c40f66e998d657b17554c9b62c97f99c0b7899a75fae0b479a42395"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CartModule.html" data-type="entity-link" >CartModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' : 'data-target="#xs-controllers-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' :
                                            'id="xs-controllers-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' }>
                                            <li class="link">
                                                <a href="controllers/CartController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CartController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' : 'data-target="#xs-injectables-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' :
                                        'id="xs-injectables-links-module-CartModule-8ae50f417b371068f9fb16da3808f5cce41f6dc08da41c8327b696d5f6986e68713c3c83542087b48a498b4b8c214681bb74bfbad28d26163902c3ae0c91014d"' }>
                                        <li class="link">
                                            <a href="injectables/CartService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CartService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CategoryModule.html" data-type="entity-link" >CategoryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' : 'data-target="#xs-controllers-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' :
                                            'id="xs-controllers-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' }>
                                            <li class="link">
                                                <a href="controllers/CategoryController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CategoryController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' : 'data-target="#xs-injectables-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' :
                                        'id="xs-injectables-links-module-CategoryModule-49d435f8f7292e50226bd4a79fe043bc6201a2e465a9e122e60bcd407cacc29c597535012cf016fd5cff6f22a4032c97c40e78d4cc73986bf39fb294588954ea"' }>
                                        <li class="link">
                                            <a href="injectables/CategoryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CategoryService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/FoodItemsModule.html" data-type="entity-link" >FoodItemsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' : 'data-target="#xs-controllers-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' :
                                            'id="xs-controllers-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' }>
                                            <li class="link">
                                                <a href="controllers/FoodItemsController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FoodItemsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' : 'data-target="#xs-injectables-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' :
                                        'id="xs-injectables-links-module-FoodItemsModule-41b741373e9d00519b7d6221739a44354b24bcb02a6f7d873f20239b13ee78c36771b842ffc8f1723405ec784ed4fc58b6e72c49bd25ca91fba2284b7bca7023"' }>
                                        <li class="link">
                                            <a href="injectables/FoodItemsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FoodItemsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PaymentModule.html" data-type="entity-link" >PaymentModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' : 'data-target="#xs-controllers-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' :
                                            'id="xs-controllers-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' }>
                                            <li class="link">
                                                <a href="controllers/PaymentController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' : 'data-target="#xs-injectables-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' :
                                        'id="xs-injectables-links-module-PaymentModule-f3865fa8898d76a39fe7478deed833bca0371fe981ba919349fe5d80a977516898583e72bdbf4af3e7d8c47359b9cc4853326115d3be71db7523471b2436abe5"' }>
                                        <li class="link">
                                            <a href="injectables/PaymentService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' : 'data-target="#xs-controllers-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' :
                                            'id="xs-controllers-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' : 'data-target="#xs-injectables-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' :
                                        'id="xs-injectables-links-module-UserModule-8de104f528ff5aba3d8860a4d7bf5e1279248dcf20d4b227a9d2df81c18c4162df854be3a7f7684fd5a2c7d30dbb89b0afbcb9f993ae766bb384e8201a294b19"' }>
                                        <li class="link">
                                            <a href="injectables/JwtToken.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtToken</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CartController.html" data-type="entity-link" >CartController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CategoryController.html" data-type="entity-link" >CategoryController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/FoodItemsController.html" data-type="entity-link" >FoodItemsController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/PaymentController.html" data-type="entity-link" >PaymentController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Cart.html" data-type="entity-link" >Cart</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Category.html" data-type="entity-link" >Category</a>
                                </li>
                                <li class="link">
                                    <a href="entities/FoodItems.html" data-type="entity-link" >FoodItems</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Payment.html" data-type="entity-link" >Payment</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserLogin.html" data-type="entity-link" >UserLogin</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/Maintainance.html" data-type="entity-link" >Maintainance</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CartService.html" data-type="entity-link" >CartService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CategoryService.html" data-type="entity-link" >CategoryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FoodItemsService.html" data-type="entity-link" >FoodItemsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtToken.html" data-type="entity-link" >JwtToken</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PaymentService.html" data-type="entity-link" >PaymentService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});